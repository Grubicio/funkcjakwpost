package com.demo.springboot.dto;

public class ResultDto {

    private final Double x1;
    private final Double x2;

    public ResultDto() {
        this.x1 = null;
        this.x2 = null;
    }

    public ResultDto(Double x1, Double x2) {
        this.x1 = x1;
        this.x2 = x2;
    }

    public Double getX1() {
        return x1;
    }

    public Double getX2() {
        return x2;
    }
}
